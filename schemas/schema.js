import createSchema from 'part:@sanity/base/schema-creator';
import schemaTypes from 'all:part:@sanity/base/schema-type';

import Extentions from "./extentions";
import General from "./general";
import Subpage from "./subpage";
import Footer from "./footer";
import Home from "./home";
import Projects from "./projects";


export default createSchema({
	// We name our schema
	name: 'default',
	// Then proceed to concatenate our document type
	// to the ones provided by any plugins that are installed
	types: schemaTypes.concat([
		...Extentions,

		Home,
		General,
		Projects,
		Subpage,
		Footer,
	])
});
