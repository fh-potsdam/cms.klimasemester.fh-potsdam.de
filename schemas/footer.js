const Home = {
    name: "footer",
    title: "Footer",
    type: "document",

    fields: [
        {
            name: "title",
            title: "Titel",
            type: "string",
            required: true
        }, {
            name: "lists",
            title: "Listen",
            type: "array",
			of: [
				{
					type: "list"
				}
			]
        }
    ]
};

export default Home;