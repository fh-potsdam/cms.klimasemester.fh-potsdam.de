const General = {
	name: "generalData",
	title: "Allgemeine Daten",
	type: "document",

	fields: [
		{
			name: "domain",
			title: "Domainname",
			type: "string",
			description: "z.B. »subdomain.fh-potsdam.de«",
			required: true
		},
		{
			name: "siteName",
			title: "Website Name",
			type: "string",
			description: "z.B. »FH;P Klimasemester«",
			required: true
		}, {
			name: "contact",
			title: "Kontaktdaten",
			type: "address",
			required: true
		}
	]
};

export default General;