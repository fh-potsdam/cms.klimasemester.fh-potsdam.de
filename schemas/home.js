const Home = {
	name: "home",
	title: "Startseite",
	type: "document",

	fields: [
		{
			name: "title",
			title: "Titel",
			type: "string",
			required: true
		}, {
			name: "description",
			title: "Beschreibung",
			type: "text",
			required: true
		}, {
			name: "seoTeaserImage",
			title: "SEO Teaserbild",
			type: "image",
			required: true
		}, {
			name: "teaser",
			title: "Teaser Daten",
			type: "teaserSection"
		}
	]
};

export default Home;