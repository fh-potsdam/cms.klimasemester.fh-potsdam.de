const Subpage = {
    name: "subpage",
    title: "Unterseiten",
    type: "document",

    fields: [
        {
            name: "title",
            title: "Titel",
            type: "string",
            required: true
        }, {
            name: "slug",
            title: "URL",
            type: "slug",
            source: "title",
            description: "z.B.: »/unterseiten-url«",
            isUnique: true,
            required: true
        }, {
            name: "description",
            title: "Beschreibung",
            type: "text",
            required: true
        }, {
            name: "content",
            title: "Inhalt (Markdown)",
            required: true,
            type: "markdown"
        }
    ]
};

export default Subpage;