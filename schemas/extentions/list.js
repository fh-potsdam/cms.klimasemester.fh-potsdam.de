const List = {
	name: "list",
	title: "Liste",
	type: "object",
	fields: [
		{
			name: "label",
			title: "Titel",
			type: "string",
			required: true
		}, {
			name: "items",
			title: "Elemente",
			type: "array",
			required: true,
			of: [
				{ type: "linkItem" }
			]
		}
	]
};

export default List;