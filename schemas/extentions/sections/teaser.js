const Teaser = {
	name: "teaserSection",
	title: "Teaser",
	type: "object",
	fields: [
		{
			name: "headline",
			title: "Überschrift",
			type: "string",
			required: true
		}, {
			name: "subline",
			title: "Unter-Überschrift",
			type: "string"
		}, {
			name: "content",
			title: "Einleitungstext",
			type: "array",
			of: [ {
				type: "block"
			} ]
		}
	]
};

export default Teaser;