const Address = {
	name: "address",
	title: "Adresse",
	type: "object",
	fields: [
		{
			name: "address",
			title: "Adresse",
			type: "text",
			required: true
		}, {
			name: "phone",
			title: "Telefon",
			type: "string",
			required: true
		}, {
			name: "mail",
			title: "E-Mail",
			type: "string",
			required: true
		}, {
			name: "location",
			title: "Ort",
			type: "geopoint",
			required: true
		}
	]
};

export default Address;