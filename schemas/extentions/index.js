import Address from "./address";
import LinkItem from "./link-item";
import List from "./list";
import Projects from "./projects";
import SocialTile from "./social-tile";
import Teaser from "./sections/teaser";

export default [
	Address,
	List,
	LinkItem,
	Projects,
	SocialTile,
	Teaser
];