const LinkItem = {
	name: "linkItem",
	title: "Link",
	type: "object",
	fields: [
		{
			name: "name",
			title: "Titel",
			type: "string",
			description: "optional",
		}, {
			name: "subpageRef",
			title: "Subpage",
			type: "reference",
			required: true,
			to: [ { type: "subpage" } ]
		}
	]
};

export default LinkItem;