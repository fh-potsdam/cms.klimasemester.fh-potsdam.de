import React from "react";
import Instagram from "react-instagram-embed";


const SocialMediaPreview = ({ value }) => {
	const { url } = value;

	if (!url) {
		return null;
	}


	if (url.startsWith("https://www.instagram.com/")) {
		return (
			<div style={ { pointerEvents: "none" } }>
				<Instagram
					url={ url }
					hideCaption={ true }
				/>
			</div>
		);
	}
};


const SocialTile = {
	name: "socialTile",
	title: "Social Media Post",
	type: "object",
	fields: [
		{
			name: "tags",
			title: "Tags",
			type: "array",
			sortable: true,
			layout: "tags",
			of: [ { type: "string" } ]
		}, {
			name: "url",
			title: "Post-Url",
			type: "string",
			description: "Link des Posts, z.B. »https://www.instagram.com/p/B_xVhEJg95f/«"
		}
	],
	preview: {
		select: { url: "url" },
		component: SocialMediaPreview
	}
};

export default SocialTile;