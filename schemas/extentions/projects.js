const Projects = {
	name: "project",
	title: "Projekt",
	type: "object",
	fields: [
		{
			name: "teaserImg",
			title: "Teaserbild",
			type: "image",
		}, {
			name: "headline",
			title: "Überschrift",
			type: "string",
			required: true
		}, {
			name: "subline",
			title: "Unter-Überschrift",
			type: "string"
		}, {
			name: "teacher",
			title: "Lehrkraft",
			type: "array",
			required: true,
			of: [
				{ type: "string" }
			]
		}, {
			name: "tags",
			title: "Tags",
			type: "array",
			sortable: true,
			layout: "tags",
			of: [ { type: "string" } ]
		}, {
			name: "content",
			title: "Inhalt",
			type: "array",
			of: [ {
				type: "block"
			} ]
		}
	]
};

export default Projects;