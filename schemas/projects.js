const Projects = {
	name: "projects",
	title: "Projekte",
	type: "document",

	fields: [
		{
			name: "specialTags",
			title: "Hervorgehobene Tags",
			type: "array",
			sortable: true,
			layout: "tags",

			of: [ { type: "string" } ]
		}, {
			name: "projectList",
			title: "Projekte",
			type: "array",
			layout: "grid",
			sortable: true,

			of: [
				{ type: "project" },
				{ type: "socialTile" },
			]
		}
	]
};

export default Projects;